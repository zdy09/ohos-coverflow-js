export default {
    data: {
        TYPE_ANIMATE: "animate", //用于控制滑动的时候item的动画效果 通过计算得到缩放 翻转 位移等
        TYPE_REFRESH_ITEM: "refreshItem", //用于滑动的时候当前item的更新，包括currentPage 背景色等的更新
        TYPE_SEND_PARENT_WIDTH: "sendParentWidth", //测试使用，因为item的宽使用百分比，item的高需要动态计算，用于保证item的宽高一致
        TYPE_UNION_SCROLL: "unionScroll", //联动滑动，一个自定义swiper的滑动驱使另一个自定义swiper同步滑动
        TYPE_SCROLLING: "scrolling", // 正在滑动
        TYPE_SCROLL_FLING: "fling", // 松手状态下滑动
        TYPE_SCROLL_END: "end", // 滑动结束
        TYPE_TOUCH_START: "touchStart", // touchEvent按下事件
        TYPE_TOUCH_MOVE: "touchMove", // touchEvent移动事件
        TYPE_TOUCH_CANCEL: "touchCancel", // touchEvent取消事件
        TYPE_TOUCH_END: "touchEnd" // touchEvent结束事件
    }
}