export class scrollParam {
    constructor(lastCenter=0, currentCenter=0, nextCentr=0, itemSpace=0, screenCenter=0) {
        this.lastCenter = lastCenter || 0;
        this.currentCenter = currentCenter || 0;
        this.nextCentr = nextCentr || 0;
        this.itemSpace = itemSpace || 0;
        this.screenCenter = screenCenter || 0;
    }
}