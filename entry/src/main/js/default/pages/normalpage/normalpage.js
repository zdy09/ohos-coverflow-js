/**
 * Copyright (c) 2021 zhaodongyang
   Ohos-Coverflow_JS is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

import router from '@system.router';

export default {
    private: {
        DEFAULT_DATA: [0, 0, 0, 0, 0],
        DEFAULT_CACHE_SIZE: 2,
        DEFAULT_CURRENT_PAGE: 0,
        cacheStart: 0, //swiper
        cacheEnd: Number.MAX_SAFE_INTEGER,
        currentMovex: 0,
        rootWidth: 0,
        rootHeight: 0,
        startX: 0,
        isMove: false,
        lists: [{
                    title: "第一页"
                }, {
                    title: "第二页"
                }, {
                    title: "第三页"
                }, {
                    title: "第四页"
                }, {
                    title: "第五页"
                }, {
                    title: "第六页"
                }, {
                    title: "第七页"
                }, {
                    title: "第八页"
                }, {
                    title: "第九页"
                }, {
                    title: "第十页"
                }, {
                    title: "第十一页"
                }, {
                    title: "第十二页"
                }, {
                    title: "第十三页"
                }, {
                    title: "第十四页"
                }, {
                    title: "第十五页"
                }, {
                    title: "第十六页"
                }], // 虽然只用到了lists的长度length，但是diyswiper.hml里面的for循环用到了lists，不可以直接改为数字
        cachePage: 1,
    },
    public: {
        currentPage: 0,
    },
    props: {
        cacheSize: 1,
        currentIndex: 0,
        swiperDatas: new Array(),
    },
    normalItemAnimator(e) {
        if (e == null || e == undefined || e.detail == null || e.detail == undefined) {
            return
        }
        //        this.$emit("itemEvent", data)
    },
    onReady() {

    },
    onBackPress() {
        router.back({
            uri: "pages/index/index"
        })
    },
    onShow:function() {
        const that = this
        this.initData()
        setTimeout(function () {
            let swiper = that.$element('swiper')
            that.rootWidth = swiper.getBoundingClientRect().width
            that.rootHeight = swiper.getBoundingClientRect().height
            that.refreshCacheData()
            for (var i = 0; i < that.lists.length; i++) {
                let diyswiperitem = that.$child('swipe-item-' + i)
                if (diyswiperitem == undefined) {
                    continue
                }
                if (i >= that.cacheStart && i <= that.cacheEnd) {
                    diyswiperitem.setItemVisible(true)
                    diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
                } else {
                    diyswiperitem.setItemVisible(false)
                }
            }
        }, 200)
    },
    onPageShow() {
        const that = this
        setTimeout(function () {
            let swiper = that.$element('swiper')
            that.rootWidth = swiper.getBoundingClientRect().width
            that.rootHeight = swiper.getBoundingClientRect().height
            this.refreshCacheData()
            for (var i = 0; i < that.lists.length; i++) {
                let diyswiperitem = that.$child('swipe-item-' + i)
                if (diyswiperitem == undefined) {
                    continue
                }
                if (i >= that.cacheStart && i <= that.cacheEnd) {
                    diyswiperitem.setItemVisible(true)
                    diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
                } else {
                    diyswiperitem.setItemVisible(false)
                }
            }
        }, 200)
    },
    getCurrentPage() {
        return this.currentPage
    },
    setCurrentPage(nowPage) {
        const that = this
        if (nowPage == undefined || that.lists.length == undefined
        || nowPage >= (that.lists.length - 1) || nowPage < 0) {
            return
        }
        that.currentPage = nowPage
        that.refreshCacheData()
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.setItemVisible(true)
                diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
            } else {
                diyswiperitem.setItemVisible(false)
            }
        }
    },
    initData() {
        const that = this
        try {
            if (that.lists == null || that.lists == undefined || that.lists.length < 1) {
                if (that.swiperDatas != null && that.swiperDatas != undefined && that.swiperDatas.length != 0) {
                    that.lists = that.swiperDatas
                } else {
                    that.lists = that.DEFAULT_DATA
                }
            }

            if (that.currentIndex != undefined && that.currentIndex != 0) {
                that.currentPage = that.currentIndex
                if (that.currentPage >= that.lists.length) {
                    that.currentPage = (that.lists.length - 1);
                }
                if (that.currentPage < 0) {
                    that.currentPage = 0
                }
            } else {
                that.currentPage = that.DEFAULT_CURRENT_PAGE
            }

            if (that.cacheSize != undefined && that.cacheSize != 0) {
                that.cachePage = that.cacheSize
                if (that.cachePage < 1) {
                    that.cachePage = 1;
                }
                if (that.cachePage >= that.lists.length) {
                    that.cachePage = (that.lists.length - 1);
                }
            } else {
                that.cachePage = that.DEFAULT_CACHE_SIZE
            }
        } catch (err) {
           // console.log("测试打印===initData===err")
            that.lists = that.DEFAULT_DATA
            that.currentPage = that.DEFAULT_CURRENT_PAGE
            that.cachePage = that.DEFAULT_CACHE_SIZE
        }
    },
    refreshCacheData() {
        try {
            const that = this
            that.cacheStart = (that.currentPage - that.cachePage) < 0 ? 0 : that.currentPage - that.cachePage
            that.cacheEnd = (that.currentPage + that.cachePage) >= (that.lists.length - 1) ? (that.lists.length - 1) : (that.currentPage + that.cachePage)
        } catch (err) {
          //  console.log("测试打印===refreshCacheData===" + err)
        }
    },
    doStart(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        this.isMove = false
        this.startX = e.touches[0].localX
        const that = this
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.startScrollAnimate(that.currentPage, 0, that.rootWidth)
            }
        }
    },
    doMove(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        const that = this
        this.isMove = true
        that.currentMovex = e.touches[0].localX - that.startX
        if (that.currentPage == 0) {
            if (that.currentMovex > 0) {
                return
            }
        } else if (that.currentPage == (that.lists.length - 1)) {
            if (that.currentMovex < 0) {
                return
            }
        }
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                if (that.currentMovex >= 0) {
                    if (i == that.currentPage) {
                        diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                    }
                    if (i == (that.currentPage - 1)) {
                        diyswiperitem.scrollComeAnimate(that.currentPage - 1, that.currentMovex, that.rootWidth)
                    }

                } else {
                    if (i == that.currentPage) {
                        diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                    }
                    if (i == (that.currentPage + 1)) {
                        diyswiperitem.scrollComeAnimate(that.currentPage + 1, that.currentMovex, that.rootWidth)
                    }
                }
                if (that.currentMovex > that.rootWidth / 3 && i == that.currentPage) {
                    let diyswiperitem = that.$child('swipe-item-' + i)
                    if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i - 2) >= 0) {
                        let item = that.$child('swipe-item-' + (i - 2))
                        item.setItemVisible(true)
                        item.offsetNewItem((i - 2), that.rootWidth, that.currentPage)
                    }
                } else if (that.currentMovex < (0 - that.rootWidth / 3) && i == that.currentPage) {
                    let diyswiperitem = that.$child('swipe-item-' + i)
                    if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i + 2) < that.lists.length) {
                        let item = that.$child('swipe-item-' + (i + 2))
                        item.setItemVisible(true)
                        item.offsetNewItem((i + 2), that.rootWidth, that.currentPage)
                    }
                }
                diyswiperitem.onScroll(that.rootWidth, that.currentPage, that.currentMovex)
            }

        }

    },
    doEnd(e) {
        const that = this
        if (e == null || e.changedTouches == null || e.changedTouches.length < 1) {
            return
        }
        if (!that.isMove) {
            return
        }
        that.currentMovex = e.changedTouches[0].localX - that.startX
        if (that.currentPage == 0) {
            if (that.currentMovex > 0) {
                return
            }
        } else if (that.currentPage == (that.lists.length - 1)) {
            if (that.currentMovex < 0) {
                return
            }
        }
        that.isMove = false
        if (that.currentMovex == 0) {
            return
        }

        let rate = (that.currentMovex / that.rootWidth)
        if (rate >= 0) {
            if (rate >= 0.5) {
                that.currentPage--
            }
        } else {
            if (rate <= -0.5) {
                that.currentPage++
            }
        }
        if (that.currentPage < 0) {
            that.currentPage = 0
        }
        if (that.currentPage > (that.lists.length - 1)) {
            that.currentPage = (that.lists.length - 1)
        }
        that.refreshCacheData()
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.setItemVisible(true)
                diyswiperitem.onScrollEnd(that.rootWidth, that.currentPage, 0)
            } else {
                diyswiperitem.setItemVisible(false)
            }
            if (rate >= 0) {
                if (i == that.currentPage) {
                    diyswiperitem.scrollComeEndAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                }
                if (i == (that.currentPage + 1)) {
                    diyswiperitem.scrollToEndAnimate((that.currentPage + 1), that.currentMovex, that.rootWidth)
                }

            } else {
                if (i == that.currentPage) {
                    diyswiperitem.scrollComeEndAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                }
                if (i == (that.currentPage - 1)) {
                    diyswiperitem.scrollToEndAnimate((that.currentPage - 1), that.currentMovex, that.rootWidth)
                }

            }
        }

    },
    doCancle(e) {

    },
}
