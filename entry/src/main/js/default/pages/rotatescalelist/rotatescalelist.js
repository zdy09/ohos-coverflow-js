/**
 * Copyright (c) 2021 zhaodongyang
   Ohos-Coverflow_JS is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

import router from '@system.router';

export default {
    data: {
        todolist: [{
                       title: '刷题',
                       date: '2021-12-31 10:00:00',
                       tempScale: 0.6,
                       tempRotate: 0,
                   }, {
                       title: '看电影',
                       date: '2021-12-31 20:00:00',
                       tempScale: 0.6,
                       tempRotate: 0,
                   }],
        currentIndex: 0,
        isLeft: false,
        itemWidth: 0,
        middleX: 0,
        startX: 0,
        currentMovex: 0,
        scrollData: {
            scrollX: 0.000,
            scrollY: 0.000,
            scrollState: 0
        },
    },
    onInit() {
        this.todolist.splice(0)
        for (var i = 0; i < 29; i++) {
            this.todolist.push({
                title: "条目" + i,
                date: "2021/09/02===" + i,
                tempScale: 0.6,
                tempRotate: 0,
            })
        }
        this.todolist[0].tempScale = 1
        this.todolist[1].tempRotate = 0.3
    },
    onBackPress() {
        router.back({
            uri: "pages/index/index"
        })
    },
    onShow() {
        const that = this
        setTimeout(function () {
            let listView = that.$element("list-view")
            let width = listView.getBoundingClientRect().width
            that.middleX = width / 2
        }, 200)
    },
    onPageShow() {

    },
    dispatchScroll(e) {
        if (e == null || e == undefined) {
            return
        }
        const that = this
        that.scrollData.scrollState = e.scrollState
        that.scrollData.scrollY = e.scrollY
        that.scrollData.scrollX = e.scrollX
        if (e.scrollState == 2) {
            that.findCurrentItem(false)
        }

    },
    findCurrentItem(isStop) {
        const that = this
        var tempCurrent = 0
        var tempOffset = that.middleX
        for (var i = 0; i < that.todolist.length; i++) {
            let item = that.$element("item-" + i)
            let left = item.getBoundingClientRect().left
            if (that.itemWidth <= 0) {
                that.itemWidth = item.getBoundingClientRect().width
                try {
                    let marginRight = parseFloat(item.classStyle.marginRight.toString().replace("px", ""))
                    that.itemWidth = that.itemWidth - marginRight
                } catch (err) {
                }
                try {
                    let marginLeft = parseFloat(item.classStyle.marginLeft.toString().replace("px", ""))
                    that.itemWidth = that.itemWidth - -marginLeft
                } catch (err) {
                }
            }
            if (left != 0) {
                let Offset = Math.abs(left + that.itemWidth / 2 - that.middleX)
                if (Offset <= tempOffset) {
                    tempOffset = Offset
                    tempCurrent = i
                }
            }
        }
        that.currentIndex = tempCurrent

        if (!isStop) {
            let item = that.$element("item-" + that.currentIndex)
            let left = item.getBoundingClientRect().left
            let offset = left + that.itemWidth / 2 - that.middleX
            let last = (that.currentIndex - 1) < 0 ? 0 : that.currentIndex - 1
            let next = (that.currentIndex + 1) >= (that.todolist.length - 1) ? (that.todolist.length - 1) : (that.currentIndex + 1)
            let changeScale = (Math.abs(offset) / that.itemWidth * 0.4) > 0.4 ? 0.4 : Math.abs(offset) / that.itemWidth * 0.4
            let changeRotate = (Math.abs(offset) / that.itemWidth * 0.3) > 0.3 ? 0.3 : Math.abs(offset) / that.itemWidth * 0.3
            if (that.currentMovex >= 0) {
                if (offset >= 0) {
                    that.todolist[last].tempScale = 0.6 + changeScale
                    that.todolist[last].tempRotate = changeRotate - 0.3
                    that.todolist[that.currentIndex].tempScale = 1 - changeScale
                    that.todolist[that.currentIndex].tempRotate = 0 - changeRotate

                } else {
                    that.todolist[that.currentIndex].tempScale = 1 - changeScale
                    that.todolist[that.currentIndex].tempRotate = 0 - changeRotate

                }


            } else {
                if (offset >= 0) {
                    that.todolist[that.currentIndex].tempScale = 1 - changeScale
                    that.todolist[that.currentIndex].tempRotate = 0 - changeScale

                } else {
                    that.todolist[that.currentIndex].tempScale = 1 - changeScale
                    that.todolist[that.currentIndex].tempRotate = changeScale
                    that.todolist[next].tempScale = 0.6 + changeScale
                    that.todolist[next].tempRotate = 0.3 - changeRotate
                }

            }
        }
    },
    dispatchTouchStart(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        this.startX = e.touches[0].localX
    },
    dispatchTouchCancel(e) {

    },
    dispatchTouchMove(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        this.currentMovex = e.touches[0].localX - this.startX
        this.findCurrentItem(false)
    },
    dispatchTouchEnd(e) {
        this.scrollToCenterAfterScroll()
    },
    scrollToCenterAfterScroll() {
        let that = this
        let listView = that.$element("list-view")
        let id = setInterval(function () {
            if (that.scrollData.scrollState == 0) {
                clearInterval(id)
                that.findCurrentItem(true)
                that.moveToCenter(listView)
            }
        }, 50)
    },
    moveToCenter(listView) {
        const that = this
        let item = that.$element("item-" + that.currentIndex)
        let left = item.getBoundingClientRect().left

        var tempScrollX = left + that.itemWidth / 2 - that.middleX
        listView.scrollBy({
            dy: 0,
            dx: tempScrollX,
            smooth: true
        })
        for (var i = 0; i < that.todolist.length; i++) {
            if (i == that.currentIndex) {
                that.todolist[i].tempScale = 1
                that.todolist[i].tempRotate = 0
            } else {
                that.todolist[i].tempScale = 0.6
            }
            if (i == that.currentIndex - 1) {
                that.todolist[i].tempRotate = -0.3
            } else if (i == that.currentIndex + 1) {
                that.todolist[i].tempRotate = 0.3
            }
        }
    },
}
