/**
 * Copyright (c) 2021 zhaodongyang
   Ohos-Coverflow_JS is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

export default {
    private: {
        currentMovex: 0,
        nowScale: 1,
        initScale: 1,
        isItemShow: false,
        scrollOffset: 0,
    },
    props: {
        currentIndex: 0,
        localIndex: 0,
        itemSpace: 0,
    },
    onPageShow() {
        const that = this
        that.$emit("eventType1", {
            type: "initIndex",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: that.currentIndex,
            moveOffset: 0,
            parentWidth: 0,
        })
    },
    getScaleValue() {
        return this.nowScale
    },
    setItemVisible(isShow) {
        this.isItemShow = isShow
    },
    refreshItems(width, currentPage) {
        const that = this
        let tempStart = that.localIndex - currentPage
        setTimeout(function () {
            if (tempStart > 0) {
                that.currentMovex = tempStart * width - that.itemSpace
            } else if (tempStart < 0) {
                that.currentMovex = (0 - Math.abs(tempStart) * width) + that.itemSpace
            } else {
                that.currentMovex = 0
            }
            that.startScrollAnimate(currentPage, 0, width)
        }, 200)
    },
    onScroll(width, currentPage, offset) {
        const that = this
        let tempStart = that.localIndex - currentPage
        if (tempStart > 0) {
            that.currentMovex = tempStart * width + offset - that.itemSpace
        } else if (tempStart < 0) {
            that.currentMovex = (0 - Math.abs(tempStart) * width) + offset + that.itemSpace
        } else {
            that.currentMovex = 0 + offset
        }
        that.currentMovex = that.scrollOffset + that.currentMovex

    },
    offsetNewItem(pageNum, width, currentPage) {
        const that = this
        if (pageNum != this.localIndex) {
            that.scrollOffset = 0
            return
        }
        let tempStart = pageNum - currentPage
        if (tempStart > 0) {
            that.scrollOffset = -that.itemSpace
        } else if (tempStart < 0) {
            that.scrollOffset = that.itemSpace
        } else {
            that.scrollOffset = 0
        }
    },
    onScrollEnd(width, currentPage, offset) {
        const that = this
        that.scrollOffset = 0
        let tempStart = that.localIndex - currentPage
        if (tempStart > 0) {
            that.currentMovex = tempStart * width + offset - that.itemSpace
        } else if (tempStart < 0) {
            that.currentMovex = (0 - Math.abs(tempStart) * width) + offset + that.itemSpace
        } else {
            that.currentMovex = 0 + offset
        }
    },
    startScrollAnimate(pageIndex, offset, width) {
        const that = this
        if (that.localIndex == pageIndex) {
            that.initScale = 1.5
        } else {
            that.initScale = 1
        }
        that.nowScale = that.initScale

        that.$emit("eventType1", {
            type: "scrollComeEndAnimate",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: pageIndex,
            moveOffset: offset,
            parentWidth: width
        })
    },
    scrollToAnimate(pageIndex, offset, width) {
        const that = this
        if (that.localIndex != pageIndex) {
            return
        }
        if (offset == 0) {
            that.initScale = 1
            that.nowScale = that.initScale
        } else {
            that.nowScale = that.initScale - Math.abs(offset) / width / 2
        }

        that.$emit("eventType1", {
            type: "scrollComeEndAnimate",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: pageIndex,
            moveOffset: offset,
            parentWidth: width
        })
    },
    scrollComeAnimate(pageIndex, offset, width) {
        const that = this
        if (that.localIndex != pageIndex) {
            return
        }
        if (offset == 0) {
            that.initScale = 1
            that.nowScale = that.initScale
        } else {
            that.nowScale = that.initScale + Math.abs(offset) / width / 2
        }

        that.$emit("eventType1", {
            type: "scrollComeAnimate",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: pageIndex,
            moveOffset: offset,
            parentWidth: width
        })
    },
    scrollToEndAnimate(pageIndex, offset, width) {
        const that = this
        if (that.localIndex != pageIndex) {
            return
        }
        that.initScale = 1
        that.nowScale = that.initScale

        that.$emit("eventType1", {
            type: "scrollToEndAnimate",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: pageIndex,
            moveOffset: offset,
            parentWidth: width
        })
    },
    scrollComeEndAnimate(pageIndex, offset, width) {
        const that = this
        if (that.localIndex != pageIndex) {
            return
        }
        that.initScale = 1.5
        that.nowScale = that.initScale

        that.$emit("eventType1", {
            type: "scrollComeEndAnimate",
            nowScale: that.nowScale,
            localIndex: that.localIndex,
            currentPage: pageIndex,
            moveOffset: offset,
            parentWidth: width
        })
    },
}
