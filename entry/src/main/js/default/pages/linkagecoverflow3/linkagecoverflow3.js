/**
 * Copyright (c) 2021 zhaodongyang
   Ohos-Coverflow_JS is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

import router from '@system.router';

export default {
    private: {
        DEFAULT_DATA: [0, 0, 0, 0, 0],
        DEFAULT_CACHE_SIZE: 2,
        DEFAULT_CURRENT_PAGE: 0,
        cacheStart: 0, //swiper
        cacheEnd: Number.MAX_SAFE_INTEGER,
        currentMovex: 0,
        currentMoveY: 0,
        cacheMoveY: 0,
        lastMoveX: 0,
        rootWidth: 0,
        rootHeight: 0,
        startX: 0,
        startY: 0,
        autoHeight: 0,
        disableScroll: true,
        listOffset: 0,
        isMove: false,
        touchType: 0,
        isTopShow: false,
        topHeight: 48,
        listTestData: [""],
        lists: [{
                    title: "第一页",
                    backColor: "#4b0082",
                    tempScale: 0.6,
                }, {
                    title: "第二页",
                    backColor: "#00ffff",
                    tempScale: 0.6,
                }, {
                    title: "第三页",
                    backColor: "#7fffd4",
                    tempScale: 0.6,
                }, {
                    title: "第四页",
                    backColor: "#ff1493",
                    tempScale: 0.6,
                }, {
                    title: "第五页",
                    backColor: "#ffd700",
                    tempScale: 0.6,
                }, {
                    title: "第六页",
                    backColor: "#ffb6c1",
                    tempScale: 0.6,
                }, {
                    title: "第七页",
                    backColor: "#000000",
                    tempScale: 0.6,
                }, {
                    title: "第八页",
                    backColor: "#0000ff",
                    tempScale: 0.6,
                }, {
                    title: "第九页",
                    backColor: "#8a2be2",
                    tempScale: 0.6,
                }, {
                    title: "第十页",
                    backColor: "#a52a2a",
                    tempScale: 0.6,
                }, {
                    title: "第十一页",
                    backColor: "#deb887",
                    tempScale: 0.6,
                }, {
                    title: "第十二页",
                    backColor: "#5e9aa0",
                    tempScale: 0.6,
                }, {
                    title: "第十三页",
                    backColor: "#7fff00",
                    tempScale: 0.6,
                }, {
                    title: "第十四页",
                    backColor: "#6495ed",
                    tempScale: 0.6,
                }, {
                    title: "第十五页",
                    backColor: "#00008b",
                    tempScale: 0.6,
                }, {
                    title: "第十六页",
                    backColor: "#8b0000",
                    tempScale: 0.6,
                }], // 虽然只用到了lists的长度length，但是diyswiper.hml里面的for循环用到了lists，不可以直接改为数字
        cachePage: 1,
        currentItem: 0,
        isLeft: false,
        itemWidth: 0,
        middleX: 0,
        scrollState: 0
    },
    public: {
        currentPage: 0,
    },
    props: {
        cacheSize: 1,
        currentIndex: 0,
        swiperDatas: new Array(),
    },
    normalItemAnimator(e) {
        if (e == null || e == undefined || e.detail == null || e.detail == undefined) {
            return
        }
        //        this.$emit("itemEvent", data)
    },
    onReady() {
        this.autoHeight = 0
        this.lists[0].tempScale = 1
        this.listTestData.splice(0)
        for (var i = 0; i < 35; i++) {
            this.listTestData.push("测试数据" + i)
        }

    },
    onBackPress() {
        router.back({
            uri: "pages/index/index"
        })
    },
    onShow() {
        const that = this
        this.initData()
        setTimeout(function () {
            let listView = that.$element("list-view")
            let width = listView.getBoundingClientRect().width
            that.middleX = width / 2

            let swiper = that.$element('swiper')
            that.rootWidth = swiper.getBoundingClientRect().width
            that.rootHeight = swiper.getBoundingClientRect().height
            that.refreshCacheData()
            for (var i = 0; i < that.lists.length; i++) {
                let diyswiperitem = that.$child('swipe-item-' + i)
                if (diyswiperitem == undefined) {
                    continue
                }
                if (i >= that.cacheStart && i <= that.cacheEnd) {
                    diyswiperitem.setItemVisible(true)
                    diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
                } else {
                    diyswiperitem.setItemVisible(false)
                }
            }
        }, 200)
    },
    onPageShow() {
        const that = this
        setTimeout(function () {
            let swiper = that.$element('swiper')
            that.rootWidth = swiper.getBoundingClientRect().width
            that.rootHeight = swiper.getBoundingClientRect().height
            that.refreshCacheData()
            for (var i = 0; i < that.lists.length; i++) {
                let diyswiperitem = that.$child('swipe-item-' + i)
                if (diyswiperitem == undefined) {
                    continue
                }
                if (i >= that.cacheStart && i <= that.cacheEnd) {
                    diyswiperitem.setItemVisible(true)
                    diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
                } else {
                    diyswiperitem.setItemVisible(false)
                }
            }
        }, 200)
    },
    getCurrentPage() {
        return this.currentPage
    },
    setCurrentPage(nowPage) {
        const that = this
        if (nowPage == undefined || that.lists.length == undefined
        || nowPage >= (that.lists.length - 1) || nowPage < 0) {
            return
        }
        that.currentPage = nowPage
        that.refreshCacheData()
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.setItemVisible(true)
                diyswiperitem.refreshItems(that.rootWidth, that.currentPage)
            } else {
                diyswiperitem.setItemVisible(false)
            }
        }
    },
    initData() {
        const that = this
        try {
            if (that.lists == null || that.lists == undefined || that.lists.length < 1) {
                if (that.swiperDatas != null && that.swiperDatas != undefined && that.swiperDatas.length != 0) {
                    that.lists = that.swiperDatas
                } else {
                    that.lists = that.DEFAULT_DATA
                }
            }

            if (that.currentIndex != undefined && that.currentIndex != 0) {
                that.currentPage = that.currentIndex
                if (that.currentPage >= that.lists.length) {
                    that.currentPage = (that.lists.length - 1);
                }
                if (that.currentPage < 0) {
                    that.currentPage = 0
                }
            } else {
                that.currentPage = that.DEFAULT_CURRENT_PAGE
            }

            if (that.cacheSize != undefined && that.cacheSize != 0) {
                that.cachePage = that.cacheSize
                if (that.cachePage < 1) {
                    that.cachePage = 1;
                }
                if (that.cachePage >= that.lists.length) {
                    that.cachePage = (that.lists.length - 1);
                }
            } else {
                that.cachePage = that.DEFAULT_CACHE_SIZE
            }
        } catch (err) {
            //  console.error("测试打印===initData===err")
            that.lists = that.DEFAULT_DATA
            that.currentPage = that.DEFAULT_CURRENT_PAGE
            that.cachePage = that.DEFAULT_CACHE_SIZE
        }
        that.lists[0].tempScale = 1
    },
    refreshCacheData() {
        try {
            const that = this
            that.cacheStart = (that.currentPage - that.cachePage) < 0 ? 0 : that.currentPage - that.cachePage
            that.cacheEnd = (that.currentPage + that.cachePage) >= (that.lists.length - 1) ? (that.lists.length - 1) : (that.currentPage + that.cachePage)
        } catch (err) {
            // console.error("测试打印===refreshCacheData===" + err)
        }
    },
    doStart(e) {
        this.touchType = 2
        this.disableScroll = true
        this.dealViewPagerStart(e)
    },
    dealViewPagerStart(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        this.isMove = false
        this.cacheMoveY = this.autoHeight
        this.startX = e.touches[0].localX
        this.startY = e.touches[0].localY
        const that = this
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.startScrollAnimate(that.currentPage, 0, that.rootWidth)
            }
        }
    },
    doMove(e) {
        this.dealViewPagerMove(e)
    },
    dealViewPagerMove(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        const that = this
        that.currentMovex = e.touches[0].localX - that.startX
        that.currentMoveY = e.touches[0].localY - that.startY
        if (Math.abs(that.currentMoveY) >= Math.abs(that.currentMovex)) {
            let trueY = that.currentMoveY + that.cacheMoveY

            if (that.currentMoveY > 0) {
                if (trueY < 0) {
                    that.autoHeight = trueY
                    that.disableScroll = true
                } else {
                    that.autoHeight = 0
                    that.disableScroll = false

                }

            } else if (that.currentMoveY < 0) {
                if (trueY > -200) {
                    that.autoHeight = trueY
                    that.disableScroll = true
                } else {
                    that.autoHeight = -200
                    that.disableScroll = false

                }
            } else {
                that.disableScroll = true
            }
            if (that.autoHeight <= (that.topHeight - 200)) {
                that.isTopShow = true
            } else {
                that.isTopShow = false
            }
            return
        } else {
            that.disableScroll = true
            if (that.currentMovex != 0) {
                that.isMove = true
            } else {
                return
            }
            if (Math.abs(that.lastMoveX - that.currentMovex) <= 5) {
                return
            }
            if (that.currentPage == 0) {
                if (that.currentMovex > 0) {
                    return
                }
            } else if (that.currentPage == (that.lists.length - 1)) {
                if (that.currentMovex < 0) {
                    return
                }
            }
            for (var i = 0; i < that.lists.length; i++) {
                let diyswiperitem = that.$child('swipe-item-' + i)
                if (i >= that.cacheStart && i <= that.cacheEnd) {
                    if (that.currentMovex >= 0) {
                        if (i == that.currentPage) {
                            diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                        }
                        if (i == (that.currentPage - 1)) {
                            diyswiperitem.scrollComeAnimate(that.currentPage - 1, that.currentMovex, that.rootWidth)
                        }

                    } else {
                        if (i == that.currentPage) {
                            diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                        }
                        if (i == (that.currentPage + 1)) {
                            diyswiperitem.scrollComeAnimate(that.currentPage + 1, that.currentMovex, that.rootWidth)
                        }
                    }
                    if (that.currentMovex > that.rootWidth / 3 && i == that.currentPage) {
                        let diyswiperitem = that.$child('swipe-item-' + i)
                        if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i - 2) >= 0) {
                            let item = that.$child('swipe-item-' + (i - 2))
                            item.setItemVisible(true)
                            item.offsetNewItem((i - 2), that.rootWidth, that.currentPage)
                        }
                    } else if (that.currentMovex < (0 - that.rootWidth / 3) && i == that.currentPage) {
                        let diyswiperitem = that.$child('swipe-item-' + i)
                        if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i + 2) < that.lists.length) {
                            let item = that.$child('swipe-item-' + (i + 2))
                            item.setItemVisible(true)
                            item.offsetNewItem((i + 2), that.rootWidth, that.currentPage)
                        }
                    }
                    diyswiperitem.onScroll(that.rootWidth, that.currentPage, that.currentMovex)
                }

            }
            if (that.touchType == 2) {
                that.scrollState = 1
                let listView = that.$element("list-view")
                let scrollX = that.lastMoveX - that.currentMovex

                that.findCurrentItem(false)
                listView.scrollBy({
                    dy: 0,
                    dx: scrollX,
                    smooth: true
                })
            }
            that.lastMoveX = that.currentMovex
        }

    },
    doEnd(e) {
        this.dealViewPagerEnd(e)
    },
    dealViewPagerEnd(e) {
        const that = this
        if (e == null || e.changedTouches == null || e.changedTouches.length < 1) {
            return
        }
        if (!that.isMove) {
            return
        }
        that.currentMovex = e.changedTouches[0].localX - that.startX
        if (that.currentPage == 0) {
            if (that.currentMovex > 0) {
                return
            }
        } else if (that.currentPage == (that.lists.length - 1)) {
            if (that.currentMovex < 0) {
                return
            }
        }
        that.isMove = false
        if (that.currentMovex == 0) {
            return
        }

        let rate = (that.currentMovex / that.rootWidth)
        if (rate >= 0) {
            if (rate >= 0.6) {
                that.currentPage--
            }
        } else {
            if (rate <= -0.6) {
                that.currentPage++
            }
        }
        if (that.currentPage < 0) {
            that.currentPage = 0
        }
        if (that.currentPage > (that.lists.length - 1)) {
            that.currentPage = (that.lists.length - 1)
        }
        that.refreshCacheData()
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.setItemVisible(true)
                diyswiperitem.onScrollEnd(that.rootWidth, that.currentPage, 0)
            } else {
                diyswiperitem.setItemVisible(false)
            }
            if (rate >= 0) {
                if (i == that.currentPage) {
                    diyswiperitem.scrollComeEndAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                }
                if (i == (that.currentPage + 1)) {
                    diyswiperitem.scrollToEndAnimate((that.currentPage + 1), that.currentMovex, that.rootWidth)
                }

            } else {
                if (i == that.currentPage) {
                    diyswiperitem.scrollComeEndAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                }
                if (i == (that.currentPage - 1)) {
                    diyswiperitem.scrollToEndAnimate((that.currentPage - 1), that.currentMovex, that.rootWidth)
                }

            }
        }
        if (this.touchType == 2) {
            that.currentItem = that.currentPage
            let listView = that.$element("list-view")
            that.moveToCenter(listView)
            this.touchType = 0
        }

    },
    doCancle(e) {
        this.touchType = 0
    },
    dispatchScroll(e) {
        const that = this
        if (e == null || e == undefined) {
            return
        }
        that.scrollState = e.scrollState
        that.findCurrentItem(false)

    },
    findCurrentItem(isStop) {
        const that = this
        var tempCurrent = 0
        var tempOffset = that.middleX
        for (var i = 0; i < that.lists.length; i++) {
            let item = that.$element("list-item-" + i)
            let left = item.getBoundingClientRect().left
            if (that.itemWidth <= 0) {
                that.itemWidth = item.getBoundingClientRect().width
                try {
                    let marginRight = parseFloat(item.classStyle.marginRight.toString().replace("px", ""))
                    that.itemWidth = that.itemWidth - marginRight
                } catch (err) {
                }
                try {
                    let marginLeft = parseFloat(item.classStyle.marginLeft.toString().replace("px", ""))
                    that.itemWidth = that.itemWidth - -marginLeft
                } catch (err) {
                }
            }
            if (left != 0) {
                let Offset = Math.abs(left + that.itemWidth / 2 - that.middleX)
                if (Offset <= tempOffset) {
                    tempOffset = Offset
                    tempCurrent = i
                    if (that.touchType == 1) {
                        that.currentMovex = (left + that.itemWidth / 2 - that.middleX) * that.rootWidth / that.itemWidth
                        that.currentPage = tempCurrent
                        that.refreshCacheData()
                    }
                }
            }
        }

        that.currentItem = tempCurrent

        if (!isStop) {
            let item = that.$element("list-item-" + that.currentItem)
            let left = item.getBoundingClientRect().left
            let offset = left + that.itemWidth / 2 - that.middleX
            let last = (that.currentItem - 1) < 0 ? 0 : that.currentItem - 1
            let next = (that.currentItem + 1) >= (that.lists.length - 1) ? (that.lists.length - 1) : (that.currentItem + 1)
            let changeScale = (Math.abs(offset) / that.itemWidth * 0.4) > 0.4 ? 0.4 : Math.abs(offset) / that.itemWidth * 0.4
            if (that.currentMovex >= 0) {
                if (offset >= 0) {
                    that.lists[last].tempScale = 0.6 + changeScale
                    that.lists[that.currentItem].tempScale = 1 - changeScale
                } else {
                    that.lists[that.currentItem].tempScale = 1 - changeScale

                }


            } else {
                if (offset >= 0) {
                    that.lists[that.currentItem].tempScale = 1 - changeScale
                } else {
                    that.lists[that.currentItem].tempScale = 1 - changeScale
                    that.lists[next].tempScale = 0.6 + changeScale
                }

            }
        }
    },
    dispatchTouchStart(e) {
        const that = this
        that.touchType = 1
        if (that.touchType == 1) {
            this.dealViewPagerStart(e)
        }
    },
    dispatchTouchCancel(e) {
        if (this.touchType == 1) {
            this.doCancle(e)
            this.touchType = 0
        }
    },
    dispatchTouchMove(e) {
        if (this.touchType == 1) {
            this.listToScrollViewPager(e)
        }
    },
    listToScrollViewPager(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        const that = this
        this.isMove = true

        if (that.currentPage == 0) {
            if (that.currentMovex > 0) {
                return
            }
        } else if (that.currentPage == (that.lists.length - 1)) {
            if (that.currentMovex < 0) {
                return
            }
        }
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                if (that.currentMovex >= 0) {
                    if (i == that.currentPage) {
                        diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                    }
                    if (i == (that.currentPage - 1)) {
                        diyswiperitem.scrollComeAnimate(that.currentPage - 1, that.currentMovex, that.rootWidth)
                    }

                } else {
                    if (i == that.currentPage) {
                        diyswiperitem.scrollToAnimate(that.currentPage, that.currentMovex, that.rootWidth)
                    }
                    if (i == (that.currentPage + 1)) {
                        diyswiperitem.scrollComeAnimate(that.currentPage + 1, that.currentMovex, that.rootWidth)
                    }
                }
                if (that.currentMovex > that.rootWidth / 3 && i == that.currentPage) {
                    let diyswiperitem = that.$child('swipe-item-' + i)
                    if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i - 2) >= 0) {
                        let item = that.$child('swipe-item-' + (i - 2))
                        item.setItemVisible(true)
                        item.offsetNewItem((i - 2), that.rootWidth, that.currentPage)
                    }
                } else if (that.currentMovex < (0 - that.rootWidth / 3) && i == that.currentPage) {
                    let diyswiperitem = that.$child('swipe-item-' + i)
                    if (diyswiperitem.getScaleValue() <= 1.25 && diyswiperitem.getScaleValue() >= 1.20 && (i + 2) < that.lists.length) {
                        let item = that.$child('swipe-item-' + (i + 2))
                        item.setItemVisible(true)
                        item.offsetNewItem((i + 2), that.rootWidth, that.currentPage)
                    }
                }
                diyswiperitem.onScroll(that.rootWidth, that.currentPage, that.currentMovex)
            }

        }
    },
    dispatchTouchEnd(e) {
        const that = this
        if (this.touchType == 1) {
            let id = setInterval(function () {
                if (that.scrollState == 0) {
                    clearInterval(id)
                    that.scrollToCenterAfterScroll()
                    that.listScrollEndToChangeViewPager(e)
                    that.touchType = 0
                }
            }, 50)

        }
    },
    listScrollEndToChangeViewPager(e) {
        const that = this
        if (e == null || e.changedTouches == null || e.changedTouches.length < 1) {
            return
        }
        that.refreshCacheData()
        for (var i = 0; i < that.lists.length; i++) {
            let diyswiperitem = that.$child('swipe-item-' + i)
            if (i >= that.cacheStart && i <= that.cacheEnd) {
                diyswiperitem.setItemVisible(true)
                diyswiperitem.onScrollEnd(that.rootWidth, that.currentPage, 0)
            } else {
                diyswiperitem.setItemVisible(false)
            }
        }

    },
    scrollToCenterAfterScroll() {
        const that = this
        let listView = that.$element("list-view")
        that.findCurrentItem(true)
        that.moveToCenter(listView)
    },
    moveToCenter(listView) {
        const that = this
        let item = that.$element("list-item-" + that.currentItem)
        let left = item.getBoundingClientRect().left

        var tempScrollX = left + that.itemWidth / 2 - that.middleX
        listView.scrollBy({
            dy: 0,
            dx: tempScrollX,
            smooth: true
        })
        for (var i = 0; i < that.lists.length; i++) {
            if (i == that.currentItem) {
                that.lists[i].tempScale = 1
            } else {
                that.lists[i].tempScale = 0.6
            }
        }
    },
    dealListScroll(e) {
        if (e == null || e == undefined) {
            return
        }
        const that = this
        that.listOffset = e.scrollY
    },
}
