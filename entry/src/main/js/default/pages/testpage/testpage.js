import {config} from "../../../common/library/config.js"
import TypeEnum from "../../../common/library/TypeEnum.js"

export default {
    data: {
        FINISH_SCALE_VALUE: 0.6,
        title: 'World',
        datas: [""],
        lastText: "上一页",
        currentText: "当前页面",
        nextText: "下一页",
        lastScale: 0,
        nowScale: 1,
        nextScale: 0,
        currentPage: 0,
        autoHeight: 0,
        itemPercent: 60,
        itemMargin: -30,
        offsetScale: 0,
    },
    onInit() {
        this.lastScale = this.FINISH_SCALE_VALUE
        this.nextScale = this.FINISH_SCALE_VALUE
        this.offsetScale = 1 - this.FINISH_SCALE_VALUE
        this.datas.splice(0)
        for (var i = 0; i < 25; i++) {
            this.datas.push("测试数据" + i)
        }

    },
    itemEvent(e) {
        const ctx = this
        if (e == null || e == undefined || e.detail == null || e.detail == undefined) {
            return
        }
        if (!(e.detail instanceof config)) {
            return
        }
        let itemData = new config(e.detail.type, e.detail.currentPage, e.detail.width, e.detail.scrollOptions, e.detail.extra)
        let type = itemData.type
        ctx.currentPage = itemData.currentPage
        let lastIndex = (ctx.currentPage - 1) < 0 ? 0 : (ctx.currentPage - 1)
        let nextIndex = (ctx.currentPage + 1) >= (ctx.datas.length - 1) ? (ctx.datas.length - 1) : (ctx.currentPage + 1)


        if (type == TypeEnum.data.TYPE_ANIMATE) {
            let scrollOptions = itemData.scrollOptions
            let itemSpace = scrollOptions.itemSpace
            let screenCenter = scrollOptions.screenCenter

            var leftRate = ctx.FINISH_SCALE_VALUE
            let leftMovex = scrollOptions.lastCenter
            if (leftMovex < (screenCenter - itemSpace)) {
                leftRate = ctx.FINISH_SCALE_VALUE
            } else {
                let leftPercent = Math.abs(leftMovex - screenCenter) * 0.4 / itemSpace
                leftRate = 1 - leftPercent
            }

            let nowMovex = scrollOptions.currentCenter
            let currentRate = 1 - Math.abs(nowMovex - screenCenter) * 0.4 / itemSpace

            var rightRate = ctx.FINISH_SCALE_VALUE
            let rightMovex = scrollOptions.nextCentr
            if (rightMovex > (screenCenter + itemSpace)) {
                rightRate = ctx.FINISH_SCALE_VALUE
            } else {
                let rightPercent = Math.abs(rightMovex - screenCenter) * 0.4 / itemSpace
                rightRate = 1 - rightPercent
            }
            ctx.lastScale = leftRate
            ctx.nowScale = currentRate
            ctx.nextScale = rightRate

        } else if (type == TypeEnum.data.TYPE_REFRESH_ITEM) {
            ctx.lastText = "第" + lastIndex + "页"
            ctx.currentText = "第" + ctx.currentPage + "页"
            ctx.nextText = "第" + nextIndex + "页"
        } else if (type == TypeEnum.data.TYPE_SEND_PARENT_WIDTH) {
            let parentWidth = itemData.extra.parentWidth
            ctx.autoHeight = parentWidth * ctx.itemPercent / 100
        }

    },
}
