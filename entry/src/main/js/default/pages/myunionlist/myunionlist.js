import {config} from "../../../common/library/config.js"
import TypeEnum from "../../../common/library/TypeEnum.js"

export default {
    data: {
        FINISH_SCALE_VALUE: 0.6,
        title: 'World',
        datas: [""],
        colorDatas: [""],
        lastScale: 0,
        nowScale: 1,
        nextScale: 0,
        lastText: "上一页",
        currentText: "当前页面",
        nextText: "下一页",
        lastBackColor: "#9932cc",
        nowBackColor: "#8b0000",
        nextBackColor: "#6495ed",
        currentPage: 0,
        autoHeight: 0,
        autoMargin: 0,
        item1Percent: 40,
        item2Percent: 100,
        itemMargin: 10,
        offsetScale: 0,
        cacheScroll: 0,
        listData: [""],
        topHeight: 48,
        disableScroll: true,
        startX: 0,
        startY: 0,
        currentMovex: 0,
        currentMoveY: 0,
        cacheMoveY: 0,
        isTopShow: false,
    },
    onInit() {
        this.lastScale = this.FINISH_SCALE_VALUE
        this.nextScale = this.FINISH_SCALE_VALUE
        this.offsetScale = 1 - this.FINISH_SCALE_VALUE
        this.datas.splice(0)
        this.listData.splice(0)
        this.colorDatas.splice(0)
        for (var i = 0; i < 25; i++) {
            this.datas.push("测试数据" + i)
        }
        for (var j = 0; j < 24; j++) {
            this.listData.push("子条目数据" + j)
        }
        this.colorDatas.push("#f0f8ff")
        this.colorDatas.push("#faebd7")
        this.colorDatas.push("#00ffff")
        this.colorDatas.push("#7fffd4")
        this.colorDatas.push("#f0ffff")
        this.colorDatas.push("#f5f5dc")
        this.colorDatas.push("#ff00ff")
        this.colorDatas.push("#dda0dd")
        this.colorDatas.push("#ffebcd")
        this.colorDatas.push("#0000ff")
        this.colorDatas.push("#8a2be2")
        this.colorDatas.push("#a52a2a")
        this.colorDatas.push("#deB887")
        this.colorDatas.push("#5f9ea0")
        this.colorDatas.push("#7fff00")
        this.colorDatas.push("#d2691e")
        this.colorDatas.push("#ff7f50")
        this.colorDatas.push("#6495ed")
        this.colorDatas.push("#fff8dc")
        this.colorDatas.push("#dc143c")
        this.colorDatas.push("#00ffff")
        this.colorDatas.push("#9acd32")
        this.colorDatas.push("#008b8b")
        this.colorDatas.push("#b8860b")
    },
    itemEvent(e) {
        const ctx = this
        if (e == null || e == undefined || e.detail == null || e.detail == undefined) {
            return
        }
        if (!(e.detail instanceof config)) {
            return
        }
        let itemData = new config(e.detail.type, e.detail.currentPage, e.detail.width, e.detail.scrollOptions, e.detail.extra)
        let type = itemData.type
        ctx.currentPage = itemData.currentPage
        let lastIndex = (ctx.currentPage - 1) < 0 ? 0 : (ctx.currentPage - 1)
        let nextIndex = (ctx.currentPage + 1) >= (ctx.datas.length - 1) ? (ctx.datas.length - 1) : (ctx.currentPage + 1)
        if (type == TypeEnum.data.TYPE_ANIMATE) {
            var swiperId = ""
            if (itemData.extra != null && itemData.extra != undefined
            && itemData.extra.id != null && itemData.extra.id != undefined) {
                swiperId = itemData.extra.id
            }
            // 当前页面仅仅上面的需要缩放 翻转
            if (swiperId == "mySwiper1") {
                let scrollOptions = itemData.scrollOptions
                let itemSpace = scrollOptions.itemSpace
                let screenCenter = scrollOptions.screenCenter

                var leftRate = ctx.FINISH_SCALE_VALUE
                let leftMovex = scrollOptions.lastCenter
                if (leftMovex < (screenCenter - itemSpace)) {
                    leftRate = ctx.FINISH_SCALE_VALUE
                } else {
                    let leftPercent = Math.abs(leftMovex - screenCenter) * 0.4 / itemSpace
                    leftRate = 1 - leftPercent
                }

                let nowMovex = scrollOptions.currentCenter
                let currentRate = 1 - Math.abs(nowMovex - screenCenter) * 0.4 / itemSpace

                var rightRate = ctx.FINISH_SCALE_VALUE
                let rightMovex = scrollOptions.nextCentr
                if (rightMovex > (screenCenter + itemSpace)) {
                    rightRate = ctx.FINISH_SCALE_VALUE
                } else {
                    let rightPercent = Math.abs(rightMovex - screenCenter) * 0.4 / itemSpace
                    rightRate = 1 - rightPercent
                }
                ctx.lastScale = leftRate
                ctx.nowScale = currentRate
                ctx.nextScale = rightRate

            }

        } else if (type == TypeEnum.data.TYPE_REFRESH_ITEM) {
            ctx.lastText = "第" + lastIndex + "页"
            ctx.currentText = "第" + ctx.currentPage + "页"
            ctx.nextText = "第" + nextIndex + "页"
            ctx.lastBackColor = ctx.colorDatas[lastIndex]
            ctx.nowBackColor = ctx.colorDatas[ctx.currentPage]
            ctx.nextBackColor = ctx.colorDatas[nextIndex]
        } else if (type == TypeEnum.data.TYPE_SEND_PARENT_WIDTH) {
            let parentWidth = itemData.extra.parentWidth
            ctx.autoHeight = parentWidth * ctx.item1Percent / 100
        } else if (type == TypeEnum.data.TYPE_UNION_SCROLL) {
            let id = itemData.extra.id
            let scrollX = itemData.extra.scrollX
            let scrollState = itemData.extra.scrollState
            if (id == "defaultId") {
                return
            }

            try {

                let mySwiper1 = ctx.$child("mySwiper1")
                let mySwiper2 = ctx.$child("mySwiper2")
                if (id == "mySwiper1") {
                    let rate = Number.parseFloat(ctx.item2Percent) / Number.parseFloat(ctx.item1Percent)
                    let trueX = (ctx.cacheScroll - scrollX) * rate
                    mySwiper2.unionScroll(trueX, "mySwiper2", scrollState)
                } else if ("mySwiper2") {
                    let rate = Number.parseFloat(ctx.item1Percent) / Number.parseFloat(ctx.item2Percent)
                    let trueX = (ctx.cacheScroll - scrollX) * rate
                    mySwiper1.unionScroll(trueX, "mySwiper1", scrollState)
                }
                if (scrollState == 0) {
                    ctx.cacheScroll = 0
                } else {
                    ctx.cacheScroll = scrollX
                }
            } catch (err) {
                console.log("测试打印===unionScrol===err===" + err)
            }
        }

    },
    doStart(e) {
        this.disableScroll = true
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        this.cacheMoveY = this.autoMargin
        this.startX = e.touches[0].localX
        this.startY = e.touches[0].localY
    },
    doMove(e) {
        if (e == null || e.touches == null || e.touches.length < 1) {
            return
        }
        const that = this
        that.currentMovex = e.touches[0].localX - that.startX
        that.currentMoveY = e.touches[0].localY - that.startY
        if (Math.abs(that.currentMoveY) >= Math.abs(that.currentMovex)) {
            let trueY = that.currentMoveY + that.cacheMoveY
            console.log("测试打印===currentMoveY===" + that.currentMoveY)
            console.log("测试打印===trueY===" + trueY)
            console.log("测试打印=============" + trueY)
            if (that.currentMoveY > 0) {
                if (trueY < 0) {
                    that.autoMargin = trueY
                    that.disableScroll = true
                } else {
                    that.autoMargin = 0
                    that.disableScroll = false

                }

            } else if (that.currentMoveY < 0) {
                if (trueY > -200) {
                    that.autoMargin = trueY
                    that.disableScroll = true
                } else {
                    that.autoMargin = -200
                    that.disableScroll = false

                }
            } else {
                that.disableScroll = true
            }
            if (that.autoMargin <= (that.topHeight - 200)) {
                that.isTopShow = true
            } else {
                that.isTopShow = false
            }
            return
        } else {
            that.disableScroll = true
            return
        }
    },
    doEnd(e) {
        const that = this
        if (e == null || e.changedTouches == null || e.changedTouches.length < 1) {
            return
        }
    },
    doCancle(e) {
    },
}
