import router from '@system.router';
import prompt from '@system.prompt';

export default {
    data: {
        array: [
            {
                id: 0,
                name: 'normal coverflow'
            },
            {
                id: 1,
                name: 'transformer coverflow'
            },
            {
                id: 2,
                name: 'transformer coverflow 2'
            },
            {
                id: 3,
                name: 'linkage coverflow'
            },
            {
                id: 4,
                name: 'linkage1 coverflow'
            },
            {
                id: 5,
                name: 'linkage2 coverflow'
            },
            {
                id: 6,
                name: 'overlapping coverflow-(暂未实现)'
            },
            {
                id: 7,
                name: '测试'
            },
            {
                id: 8,
                name: '双向联动'
            },
            {
                id: 9,
                name: '双向列表联动'
            },
        ],
    },
    changeText: function (index) {
        switch (index) {
            case 0:
                router.push({
                    uri: "pages/normalpage/normalpage"
                })
                break
            case 1:
                router.push({
                    uri: "pages/normalscalepage/normalscalepage"
                })
                break
            case 2:
                router.push({
                    uri: "pages/rotatescalelist/rotatescalelist"
                })
                break
            case 3:
                router.push({
                    uri: "pages/linkagecoverflow1/linkagecoverflow1"
                })

                break
            case 4:
                router.push({
                    uri: "pages/linkagecoverflow2/linkagecoverflow2"
                })

                break
            case 5:
                router.push({
                    uri: "pages/linkagecoverflow3/linkagecoverflow3"
                })
                break
            case 6:
                prompt.showToast({
                    message: "敬请期待",
                    duration: 4000
                })
                break
            case 7:
                router.push({
                    uri: "pages/testpage/testpage"
                })
                break
            case 8:
                router.push({
                    uri: "pages/unionlist/unionlist"
                })
                break
            case 9:
                router.push({
                    uri: "pages/myunionlist/myunionlist"
                })
                break

            default:
                break
        }
    },
}