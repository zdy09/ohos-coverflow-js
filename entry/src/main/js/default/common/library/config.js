import {scrollParam} from "../library/scrollParam.js"
import TypeEnum from "../library/TypeEnum.js"

export class config {
    constructor(type=TypeEnum.data.TYPE_REFRESH_ITEM, currentPage, width, scrollOptions=new scrollParam(0, 0, 0), extra={}) {
        this.type = type || TypeEnum.data.TYPE_REFRESH_ITEM;
        this.currentPage = currentPage || 0;
        this.width = width || 1080;
        this.scrollOptions = scrollOptions || new scrollParam(0, 0, 0);
        this.extra = extra || {};
    }
}