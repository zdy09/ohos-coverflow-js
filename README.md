# Ohos-Coverflow_JS

# 项目介绍
A beautiful cover flow for harmonyOS ACE1.0, Similar to Swiper.

系统组件swiper存在局限性，在滑动的时候添加动画会导致页面闪烁，里面的组件位置错乱，而且没有移动距离的监听。通过自定义swiper组件，
目前已实现左右滑动翻页，左右边距效果，页面随着滑动缩放翻转等动画效果，已实现swiper与list联动滑动效果，以及联动滑动头部折叠展开效果。



# 效果演示

   | [Normal-CoverFlow] | [transformer-CoverFlow] | 
   |:-:|:-:|
   | ![normal_link] | ![transformer_link] |
   
   
  
   
   | [transformer-CoverFlow 2] | [linkage-CoverFlow] |  
   |:-: |:-:|
   | ![transformer_link2]  | ![linkage_link] |
   
   
   
  
  
   

   | [linkage1-CoverFlow] | [linkage2-CoverFlow] |  
   |:-:|:-:|
   | ![linkage1_link] | ![linkage2_link] |
   
   
   
   
   
   
   
# 使用说明
- tips.使用了JS API自定义组件里面的slot插槽，由于name和slot属性不支持绑定动态数据，所以无法像下面这样使用自定义的diywiper组件。否则swiper里面的页面都是相同的，改动某一个，其他的也都会一起改动。
错误的使用方式：
library：
```hml
<element name="diyswiper" src="../diyswiper/diyswiper.hml"></element>
<element name="diyswiperitem" src="../diyswiperitem/diyswiperitem.hml"></element>
<div class="container">
    <diyswiper>
        <block for="{{ lists }}">
                    <diyswiperitem id="swipe-item-{{ index }}"
                                    @event-type1="itemAnimator"
                                   local-index="{{ $idx }}"
                                   current-index="{{ 0 }}"
                                   item-space="{{ 60 }}"
                                 >
                        <div slot="item-content" id="slot-{{ index }}">
        
                          <slot name="slot-true-item-{{index}}"></slot>
        
                        </div>
                    </diyswiperitem>
        </block>
      </diyswiper>
    
</div>

```

引用：
```hml
<element name="diyswiper" src="../diyswiper/diyswiper.hml"></element>
<div class="container">
    <diyswiper
            id="mySwiper"
            current-index="{{ 3 }}" swiper-datas="{{ lists }}" cache-size="{{ 2 }}"
            @item-event="dispatchItemEvent">

         <block for="{{ lists }}">
          <text slot="slot-true-item-{{ $idx }}"id="tv-id">{{ "测试数据" }}</text>
        </block>



    </diyswiper>
</div>

```

正确的使用方式如下：

### 1 HML方面  
需要使用哪种效果，自己的界面需要复制该效果hml部分的代码，将最里层的组件替换为需要的布局，其他部分不需要改动。如下：
最里层id为itemTv的text可自由替换为自己需要的组件。

```hml
<element name="diyswiperitem" src="../diyswiperitem/diyswiperitem.hml"></element>
<div class="container">
    <stack class="swiperRoot"
           id="swiper"
           on:touchstart.bubble="doStart"
           on:touchmove.bubble="doMove"
           on:touchend.bubble="doEnd"
           on:touchcancel.bubble="doCancle">


        <block for="{{ (index, value) in lists }}">
            <diyswiperitem id="swipe-item-{{ index }}"
                           current-index="{{ 0 }}"
                           item-space="{{ 60 }}"
                           @event-type1="itemEvent"
                           local-index="{{ index }}">
                <div slot="item-content"
                     id="slot-{{ index }}"
                     style="width : 100%; height : 100%; align-content : center; justify-content : center;
                             align-items : center; flex-direction : column; display : flex;">

                    <text id="itemTv"
                          style="width : 80%; height : 60%; text-align : center; color : white; background-color : red;
                                  font-size : 16px;
                                  transform :   matrix({{ $item.tempScale }}, {{ $item.tempRotate }}, {{ 0 }}, {{ $item.tempScale }}, {{ 0 }},
                                                                                  {{ 0 }});">
                        {{ value.title }}
                    </text>

                </div>
            </diyswiperitem>

        </block>


    </stack>
</div>
```
- 1.需要的缩放、翻转效果由matrix里面的参数tempScale、tempRotate来实现。
- 2.item-space属性控制自定义swiper两个页面之间的左右边距
- 3.@event-type1="normalItemAnimator" 自定义swiper子页面的滑动，初始化等回调事件，通过自定义参数里的type来区分，例如：

```javascript
    itemEvent(e) {
        if (e == null || e == undefined || e.detail == null || e.detail == undefined) {
            return
        }
        const that = this
        let data = e.detail
        let index = data.localIndex
        let currentPage = data.currentPage
        let type = data.type
        if (type == "scrollComeEndAnimate" && index == currentPage) {
            that.lists[index].itemScale = 1
        } else {
            that.lists[index].itemScale = 0.8
        }

        //  this.$emit("itemEvent", data)
    },

```


- 4.current-index属性控制自定义swiper当前页面
- 5.local-index属性控制自定义swiper子页面的position



### 2 JS方面  
需要使用哪种效果，自己的界面需要复制该效果js部分的代码，将js文件里面的数据list部分替换为自己的数据。通过list里面的自定义属性来控制改变动画效果。

### 3 CSS方面  
需要使用哪种效果，自己的界面需要复制该效果css部分的代码，自定义替换部分需要自己添加。



   
   
   
   
[normal_link]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/normal_link.gif>
[transformer_link]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/transformer_link.gif>
[transformer_link2]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/transformer_link2.gif>
[linkage_link]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/linkage_link.gif>
[linkage1_link]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/linkage1_link.gif>
[linkage2_link]: <https://gitee.com/zdy09/ohos-coverflow-js/raw/master/screenshot/linkage2_link.gif>